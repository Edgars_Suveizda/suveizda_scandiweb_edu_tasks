var config = {
    map: {
        '*': {
            mySlider: 'js/jquery.bxslider'
        }
    },
    shim: {
        magnificPopup: {
            deps: ['jquery']
        }
    }
};

require(["jquery"],function ($) {
    $(window).scroll(function () {
        if($(window).scrollTop()>399&&!($(".page-header").hasClass("sticky"))){
            $(".page-header").addClass("sticky");
        }else{
            $(".page-header").removeClass("sticky");
        }
    })
});