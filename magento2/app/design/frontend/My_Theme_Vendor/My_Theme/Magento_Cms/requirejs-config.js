var config = {
    map: {
        '*': {
            mySlider: 'web/js/jquery.bxslider'
        }
    },
    shim: {
        magnificPopup: {
            deps: ['jquery']
        }
    }
};