<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace My_Model_Vendor\My_Model\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */

    protected $my_config_interface;

    public function __construct(\Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface;){
    	$this->my_config_interface = $configInterface;
    }
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

       /*currency configs*/
        $this->my_config_interface->saveConfig('currency/options/base',
			'EUR', 
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
		);
		
		 $this->my_config_interface->saveConfig('currency/options/defaut',
			'EUR', 
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
		);
		
		 $this->my_config_interface->saveConfig('currency/options/allow',
			'GBP', 
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
		);
		/*language configs*/
		
		/*disable html suffix*/
		$this->my_config_interface->saveConfig('catalog/seo/product_url_suffix',
			"", 
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT, 
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
		);
        $installer->endSetup();
    }
}
