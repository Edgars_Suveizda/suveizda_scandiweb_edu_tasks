define([
    'jquery',
	'Magento_Ui/js/lib/validation/validator'
], function ($) {
    "use strict";

    return function () {
        $.validator.addMethod(
            'validate-personal-id',
            function (value) {
                return Validation.get('IsEmpty').test(value) || "^\d{6}[-]\d{5}$".test(value)
        
            },
            $.mage.__('Please enter valid personal Id')
        );
    }
});