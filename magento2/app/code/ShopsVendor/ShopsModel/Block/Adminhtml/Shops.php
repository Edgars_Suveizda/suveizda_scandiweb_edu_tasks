<?php
namespace ShopsVendor\ShopsModel\Block\Adminhtml;
class Shops extends \Magento\Backend\Block\Widget\Grid\Container
{
	protected function _construct()
	{
		$this->_controller = 'adminhtml_shoppage';
		$this->_blockGroup = 'ShopsVendor_ShopsModel';
		$this->_headerText = __('Shops');
		$this->_addButtonLabel = __('Add New Shop');
		parent::_construct();
	}
}
?>