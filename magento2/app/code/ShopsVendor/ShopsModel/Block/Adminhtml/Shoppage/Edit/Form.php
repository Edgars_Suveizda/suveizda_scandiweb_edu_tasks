<?php
  namespace ShopsVendor\ShopsModel\Block\Adminhtml\Shoppage\Edit;

 
 
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
 
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
 
    protected $_status;
 
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }
 
    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('shops_form');
        $this->setTitle(__('Shops Information'));
    }
 
    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Magebuzz\Staff\Model\Grid $model */
        $model = $this->_coreRegistry->registry('shopsmodel_shoppage');
 
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
 
        $form->setHtmlIdPrefix('shops_');
 
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Shop Data'), 'class' => 'fieldset-wide']
        );
 
        if ($model->getId()) {
            $fieldset->addField('shop_id', 'hidden', ['name' => 'shop_id']);
        }
 
        $fieldset->addField(
            'shop_name',
            'text',
            ['name' => 'shop_name', 'label' => __('Shop Name'), 'title' => __('Shop Name'), 'required' => true]
        );

         $fieldset->addField(
            'shop_address',
            'text',
            ['name' => 'shop_address', 'label' => __('Shop Address'), 'title' => __('Shop Adress'), 'required' => true]
        );

        $fieldset->addField(
            'working_hours_monday',
            'text',
            ['name' => 'working_hours_monday', 'label' => __('Monday working hours'), 'title' => __('Monday working hours'), 'required' => true]
        );

          $fieldset->addField(
            'working_hours_tuesday',
            'text',
            ['name' => 'working_hours_tuesday', 'label' => __('Tuesday working hours'), 'title' => __('Tuesday working hours'), 'required' => true]
        );

         $fieldset->addField(
            'working_hours_wednesday',
            'text',
            ['name' => 'working_hours_wednesday', 'label' => __('Wednesday working hours'), 'title' => __('Wednesday working hours'), 'required' => true]
        );

         $fieldset->addField(
            'working_hours_thursday',
            'text',
            ['name' => 'working_hours_thursday', 'label' => __('Thursday working hours'), 'title' => __('Thursday working hours'), 'required' => true]
        );

         $fieldset->addField(
            'working_hours_friday',
            'text',
            ['name' => 'working_hours_friday', 'label' => __('Friday working hours'), 'title' => __('Friday working hours'), 'required' => true]
        );

         $fieldset->addField(
            'working_hours_saturday',
            'text',
            ['name' => 'working_hours_saturday', 'label' => __('Saturday working hours'), 'title' => __('Saturday working hours'), 'required' => true]
        );

         $fieldset->addField(
            'working_hours_sunday',
            'text',
            ['name' => 'working_hours_sunday', 'label' => __('Sunday working hours'), 'title' => __('Sunday working hours'), 'required' => true]
        );

         $fieldset->addField(
            'working_hours_summary',
            'text',
            ['name' => 'working_hours_summary', 'label' => __('Summary working hours'), 'title' => __('Summary working hours'), 'required' => true]
        );

         
        
       
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
 
        return parent::_prepareForm();
    }
}
?>
