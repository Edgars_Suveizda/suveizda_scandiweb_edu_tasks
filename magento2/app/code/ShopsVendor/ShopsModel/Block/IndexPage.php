<?php
namespace ShopsVendor\ShopsModel\Block;
class IndexPage extends \Magento\Framework\View\Element\Template
{
	protected $_shopsFactory;
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\ShopsVendor\ShopsModel\Model\ShopsFactory $shopsFactory
	)
	{
		$this->_shopsFactory = $shopsFactory;
		parent::__construct($context);
	}

	public function sayHello()
	{
		return __('Hello World');
	}

	public function getShopsCollection(){
		$shop = $this->_shopsFactory->create();
		return $shop->getCollection();
	}
}
?>