<?php
namespace ShopsVendor\ShopsModel\Model;
class Shops extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'ShopsVendor_ShopsModel_ShopsInfo';

	protected $_cacheTag = 'ShopsVendor_ShopsModel_ShopsInfo';

	protected $_eventPrefix = 'ShopsVendor_ShopsModel_ShopsInfo';

	protected function _construct()
	{
		$this->_init('ShopsVendor\ShopsModel\Model\ResourceModel\Shops');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}
?>