<?php
namespace ShopsVendor\ShopsModel\Model\ResourceModel\Shops;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'shop_id';
	protected $_eventPrefix = 'ShopsVendor_ShopsModel_ShopsInfo_collection';
	protected $_eventObject = 'shops_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('ShopsVendor\ShopsModel\Model\Shops', 'ShopsVendor\ShopsModel\Model\ResourceModel\Shops');
	}

}
?>