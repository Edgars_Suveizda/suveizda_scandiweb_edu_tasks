<?php
namespace ShopsVendor\ShopsModel\Model\ResourceModel;


class Shops extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('ShopsVendor_ShopsModel_ShopsInfo', 'shop_id');
	}
	
}
?>