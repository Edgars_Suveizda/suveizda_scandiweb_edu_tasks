<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace ShopsVendor\ShopsModel\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
       $installer = $setup;
		$installer->startSetup();
		if (!$installer->tableExists('ShopsVendor_ShopsModel_ShopsInfo')) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('ShopsVendor_ShopsModel_ShopsInfo')
			)
				->addColumn(
					'shop_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'Shop ID'
				)
				->addColumn(
					'shop_name',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable => false'],
					'Shop Name'
				)
				->addColumn(
					'shop_address',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable => false'],
					'Shop Address'
				)
				->addColumn(
					'working_hours_monday',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Monday working hours'
				)
				->addColumn(
					'working_hours_tuesday',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Tuesday working hours'
				)
				->addColumn(
					'working_hours_wednesday',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Wednesday working hours'
				)
				->addColumn(
					'working_hours_thursday',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Thursday working hours'
				)
				->addColumn(
					'working_hours_friday',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Friday working hours'
				)
				->addColumn(
					'working_hours_saturday',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Saturday working hours'
				)
				->addColumn(
					'working_hours_sunday',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Sunday working hours'
				)
				->addColumn(
					'working_hours_summary',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Summary working hours'
				)
				->setComment('Shops Table');
			$installer->getConnection()->createTable($table);

			$installer->getConnection()->addIndex(
				$installer->getTable('ShopsVendor_ShopsModel_ShopsInfo'),
				$setup->getIdxName(
					$installer->getTable('ShopsVendor_ShopsModel_ShopsInfo'),
					['shop_name', 'shop_address', 'working_hours_monday', 'working_hours_tuesday', 'working_hours_wednesday', 'working_hours_thursday', 'working_hours_friday', 'working_hours_saturday', 'working_hours_sunday', 'working_hours_summary'],
					\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
				),
				['shop_name', 'shop_address', 'working_hours_monday', 'working_hours_tuesday', 'working_hours_wednesday', 'working_hours_thursday', 'working_hours_friday', 'working_hours_saturday', 'working_hours_sunday', 'working_hours_summary'],
				\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
			);
		}
		$installer->endSetup();

    }
}
?>