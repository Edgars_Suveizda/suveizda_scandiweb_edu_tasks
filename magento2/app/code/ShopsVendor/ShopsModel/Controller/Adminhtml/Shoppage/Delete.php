<?php
  namespace ShopsVendor\ShopsModel\Controller\Adminhtml\Shoppage;

use Magento\Framework\Exception\LocalizedException;
use Magento\Backend\App\Action;
 
class Delete extends \Magento\Backend\App\Action
{
    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context)
    {
        parent::__construct($context);
    }
 
    /**
     * @return void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('shop_id');
        if ($id) {
            try {
                /** @var \Magebuzz\Staff\Model\Grid $model */
                $model = $this->_objectManager->create('ShopsVendor\ShopsModel\Model\Shops');
                $model->load($id);
                $model->delete();
                $this->_redirect('*/*/');
                $this->messageManager->addSuccess(__('Shop deleted!!!!!.'));
                return;
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError(
                    __('We can\'t delete this shop right now. Please review the log and try again.')
                );
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
                return;
            }
        }
        $this->messageManager->addError(__('We can\'t find a rule to delete.'));
        $this->_redirect('*/*/');
    }
}
?>
