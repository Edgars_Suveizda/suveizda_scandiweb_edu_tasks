<?php

namespace ShopsVendor\ShopsModel\Controller\Adminhtml\Shoppage;

use Magento\Framework\Exception\LocalizedException;
use Magento\Backend\App\Action;

class massDelete extends \Magento\Backend\App\Action
{
   /**
    * @return void
    */
   public function execute()
   {
      // Get IDs of the selected news
      $shopIds = $this->getRequest()->getParam('ids');

      if($shopIds !== null){

        foreach ($shopIds as $shopId) {
            try {
               /** @var $newsModel \Mageworld\SimpleNews\Model\News */
                $shopModel = $this->_objectManager->create('ShopsVendor\ShopsModel\Model\Shops');
                $shopModel->load($shopId)->delete();
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        if (count($shopIds)) {
            $this->messageManager->addSuccess(
                __('A total of %1 record(s) were deleted.', count($shopIds))
            );
        }
      }
      else{
              $this->messageManager->addError(__('Please select shop(s). %1',count($shopIds)));
      }

        $this->_redirect('*/*/index');
   }
}
?>