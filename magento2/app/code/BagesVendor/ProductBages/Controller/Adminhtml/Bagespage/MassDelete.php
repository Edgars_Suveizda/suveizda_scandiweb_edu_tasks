<?php
  namespace BagesVendor\ProductBages\Controller\Adminhtml\Bagespage;

use Magento\Framework\Exception\LocalizedException;
use Magento\Backend\App\Action;

class MassDelete extends \Magento\Backend\App\Action
{
	
	public function __construct(Action\Context $context)
    {
        parent::__construct($context);
    }
	/**
	* @var \Magento\Framework\View\Result\PageFactory
	*/
	public function execute()
	{

		$ids = $this->getRequest()->getParam('ids');
		if (!$ids) {
			$this->messageManager->addError(__('Please select bage(s). %1',count($ids)));
		} else {
			try {
				foreach ($ids as $id) {
					$row = $this->_objectManager->get('BagesVendor\ProductBages\Model\Bages')->load($id);
					$row->delete();
				}
				$this->messageManager->addSuccess(
				__('A total of %1 record(s) have been deleted.', count($ids))
				);
			} catch (\Exception $e) {
				$this->messageManager->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/');
	}
}