<?php
namespace BagesVendor\ProductBages\Model\ResourceModel\Bages;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'bages_id';
	protected $_eventPrefix = 'BagesVendor_ProductBages_BagesInfo_collection';
	protected $_eventObject = 'bages_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('BagesVendor\ProductBages\Model\Bages', 'BagesVendor\ProductBages\Model\ResourceModel\Bages');
	}

}
?>