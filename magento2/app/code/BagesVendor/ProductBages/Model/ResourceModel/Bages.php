<?php
namespace BagesVendor\ProductBages\Model\ResourceModel;


class Bages extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('BagesVendor_ProductBages_BagesInfo', 'bages_id');
	}
	
}
?>