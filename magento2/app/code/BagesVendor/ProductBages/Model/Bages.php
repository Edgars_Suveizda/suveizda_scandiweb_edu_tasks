<?php
namespace BagesVendor\ProductBages\Model;
class Bages extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'BagesVendor_ProductBages_BagesInfo';

	protected $_cacheTag = 'BagesVendor_ProductBages_BagesInfo';

	protected $_eventPrefix = 'BagesVendor_ProductBages_BagesInfo';

	protected function _construct()
	{
		$this->_init('BagesVendor\ProductBages\Model\ResourceModel\Bages');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}
?>