<?php
namespace BagesVendor\ProductBages\Model\Attribute\Source;

class BagesSelect extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * Get all options
     * @return array
     */
    public function getAllOptions()
    {
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$bages = $objectManager->get('BagesVendor\ProductBages\Model\ResourceModel\Bages\CollectionFactory');
    	$collection = $bages->create()->load();
    	$this->_options = array();
        if ($this->_options == null) {
        	array_push($this->_options, ['label' => __('None'), 'value' => null]);
        	foreach ($collection as $bage) {
        		if ($bage->getStatus()==1) {
		           array_push($this->_options, ['label' => __($bage->getName()), 'value' => $bage->getId()]);
        		}
        	}
            
        }
        return $this->_options;
    }
}
?>