<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace BagesVendor\ProductBages\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
       $installer = $setup;
		$installer->startSetup();
		if (!$installer->tableExists('BagesVendor_ProductBages_BagesInfo')) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('BagesVendor_ProductBages_BagesInfo')
			)
				->addColumn(
					'bages_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'Bage ID'
				)
				->addColumn(
					'name',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable => false'],
					'Bage Name'
				)

				
				->addColumn(
					'status',
					\Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
					1,
					[],
					'Bage Status'
				)
				->addColumn(
					'bage_image',
					\Magento\Framework\DB\Ddl\Table::TYPE_BLOB,
					null,
					[],
					'Bage Image'
				)
				->setComment('Product Bages Table');
			$installer->getConnection()->createTable($table);

			$installer->getConnection()->addIndex(
				$installer->getTable('BagesVendor_ProductBages_BagesInfo'),
				$setup->getIdxName(
					$installer->getTable('BagesVendor_ProductBages_BagesInfo'),
					['name', 'status', 'bage_image'],
					\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
				),
				['name', 'status', 'bage_image'],
				\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
			);
		}
		$installer->endSetup();

    }
}
?>