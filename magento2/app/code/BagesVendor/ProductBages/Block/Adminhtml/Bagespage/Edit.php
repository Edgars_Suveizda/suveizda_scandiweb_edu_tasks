<?php
  namespace BagesVendor\ProductBages\Block\Adminhtml\Bagespage;

 
 
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;
 
    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }
 
    /**
     * Initialize staff grid edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'bages_id';
        $this->_blockGroup = 'BagesVendor_ProductBages';
        $this->_controller = 'adminhtml_bagespage';
 
        parent::_construct();
 
        
            $this->buttonList->update('save', 'label', __('Save Bage'));
            $this->buttonList->add(
                'saveandcontinue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ]
                ],
                -100
            );
        
 
       
            $this->buttonList->update('delete', 'label', __('Delete Bage'));
       
    }
 
    /**
     * Retrieve text for header element depending on loaded blocklist
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('productbages_bagespage')->getId()) {
            return __("Edit Bage '%1'", $this->escapeHtml($this->_coreRegistry->registry('productbages_bagespage')->getTitle()));
        } else {
            return __('New Bage');
        }
    }
 
    
 
    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('productbages/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }
}
?>
