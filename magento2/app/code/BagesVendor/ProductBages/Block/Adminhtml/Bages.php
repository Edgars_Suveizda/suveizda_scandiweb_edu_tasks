<?php
  namespace BagesVendor\ProductBages\Block\Adminhtml;

 
 
class Bages extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_bagespage';
        $this->_blockGroup = 'BagesVendor_ProductBages';
        $this->_headerText = __('Bages');
        $this->_addButtonLabel = __('Add New Bage');
        parent::_construct();
    }
}
?>
